@component(isset($user)? 'patient.layouts.inside':'patient.layouts.default')
    @slot('title') {{isset($user)? 'Meus dados':'Cadastrar  Usuário'}} @endslot
    <section class="cadastro" id="cadastro">
        <div class="container">
            <h1 class="text-center">{{isset($user)? 'Editar ':'Cadastrar '}} Usuário</h1>
            @if(count($errors) > 0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{action($action)}}" method="post">
                @csrf
                <input type="hidden" name="CO_USUARIO" value="{{$user->CO_USUARIO or ''}}">
                <input type="hidden" name="CO_CONTATO" value="{{$user->CO_CONTATO or ''}}">
                <div class="form-group form-paciente">
                    <div class="row">
                        <div class="col-md-6 com-sm-12">
                            <label>Nome *:</label>
                            <input type="text" class="form-control" id="nome_paciente" name="nome"
                                   value="{{$user->NOME or ''}}"
                                   placeholder="Digite o seu nome" required>
                        </div>
                        <div class="col-md-6 com-sm-12">
                            <label>Sobrenome *:</label>
                            <input type="text" class="form-control" id="sobre_nome_paciente"
                                   value="{{$user->SOBRENOME or ''}}"
                                   name="sobrenome" placeholder="Digite o seu sobrenome" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 com-sm-12">
                            <label for="rg-paciente">RG *:</label>
                            <input type="text" class="form-control" id="rg-paciente" placeholder="Informe seu RG"
                                   value="{{$user->RG or ''}}"
                                   {{isset($user->RG)? ' disabled ':''}}
                                   name="rg" required>
                        </div>
                        <div class="col-md-6 com-sm-12">
                            <label for="cpf-paciente">CPF *:</label>
                            <input class="form-control" id="cpf-paciente" name="cpf" maxlength="11"
                                   placeholder="Informe seu CPF"
                                   value="{{$user->CPF or ''}}"
                                   {{isset($user->CPF)? ' disabled ':''}}
                                   title="Digite apenas numeros" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 com-sm-12">
                            <label for="data-nasc-paciente">Data de Nascimento *:</label>
                            <input type="date" class="form-control" id="data-nasc-paciente"
                                   value="{{$user->ST_NASCIMENTO or ''}}"
                                   name="dt_nascimento" required>
                        </div>
                        <div class="col-md-6 com-sm-12">
                            <label for="sexo-paciente">Sexo *:</label>
                            <select class="form-control" id="sexo-paciente" name="sexo" required>
                                <option value="">Selecione o Sexo</option>
                                <option value="F"
                                        {{isset($user->SEXO) && ($user->SEXO == 'F') ?' selected ':''}}
                                >Feminino
                                </option>
                                <option value="M"
                                        {{isset($user->SEXO) && ($user->SEXO == 'M') ?' selected ':''}}
                                >Masculino
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 com-sm-12">
                        <label for="tipo_sanguineo">Tipo Sanguíneo *:</label>
                        <select class="form-control" id="tipo_sanguineo"
                                name="tipo_sanguineo" required>
                            <option value="">Selecione seu tipo</option>
                            @foreach($tipoSanguineo as $tipo)
                                <option value="{{$tipo->CO_TIPO_SANGUINEO}}"
                                        {{isset($user->CO_TIPO_SANGUINEO) && $user->CO_TIPO_SANGUINEO == $tipo->CO_TIPO_SANGUINEO ? ' selected ':''}}>
                                    {{$tipo->TIPO}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 com-sm-12">
                        <label for="problemas_saude">Problemas de Saúde:</label>
                        <select class="form-control" id="problemas_saude" name="problema_saude">
                            <option value="">Nenhum</option>
                            @foreach($problemasSaude as $problema)
                                <option value="{{$problema->CO_PROBLEMA_SAUDE}}"
                                    {{isset($user->CO_PROBLEMA_SAUDE) &&
                                    $user->CO_PROBLEMA_SAUDE == $problema->CO_PROBLEMA_SAUDE ? ' selected ':''}}>
                                    {{$problema->PROBLEMA}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 com-sm-12">
                        <label for="cep-paciente">CEP *:</label>
                        <input type="text" class="form-control" id="cep-paciente"
                               value="{{$user->CEP or ''}}"
                               name="cep" maxlength="8" required>
                    </div>
                    <div class="col-md-3 com-sm-12">
                        <label for="uf">UF *:</label>
                        <select class="form-control uf" id="uf" name="estado">
                            <option value="">Selecione a UF</option>
                            @foreach($estados as $estado)
                                <option value="{{$estado->CO_ESTADO}}"
                                        {{isset($user->CO_ESTADO) &&
                                        $user->CO_ESTADO == $estado->CO_ESTADO ? ' selected ':''}}>
                                {{$estado->UF}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-6 com-sm-12">
                        <label for="cidade">Cidade *:</label>
                        <input type="hidden" id="coCidade" value="{{$user->CO_CIDADE or ''}}">
                        <select class="form-control cidade" id="cidade" name="cidade" required>
                            <option value="">Selecione a Cidade</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 com-sm-12">
                        <label for="end-paciente">Endereço *:</label>
                        <input type="text" class="form-control" id="end-paciente"
                               value="{{$user->ENDERECO or ''}}"
                               name="endereco" required>
                    </div>
                    <div class="col-md-6 com-sm-12">
                        <label for="bairro">Bairro *:</label>
                        <input type="text" class="form-control" id="bairro"
                               value="{{$user->BAIRRO or ''}}"
                               name="bairro" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 com-sm-12">
                        <label for="email_paciente">E-mail *:</label>
                        <input type="email" class="form-control" id="email_paciente"
                               value="{{$user->EMAIL or ''}}"
                               placeholder="Digite o seu e-mail" name="email" required>
                    </div>
                    <div class="col-md-6 com-sm-12">
                        <label for="tel_paciente">Telefone *:</label>

                        <input type="text" class="form-control" id="tel_paciente"
                               value="{{$user->NU_TELEFONE or ''}}"
                               name="telefone">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 com-sm-12">
                        <label for="senha_paciente">Senha {{isset($user)? '':' *'}}:</label>
                        <input type="password" class="form-control" id="senha_paciente" name="senha"
                               placeholder="Digite sua Senha"
                               {{isset($user)? '':' required'}}>
                    </div>
                    <div class="col-md-6 com-sm-12">
                        <label for="confirmar_senha_paciente">Confirmar Senha {{isset($user)? '':' *'}}:</label>
                        <input type="password" class="form-control" id="confirmar_senha_paciente" name="confirmar_senha"
                               placeholder="Confirme sua Senha"
                                {{isset($user)? '':' required'}}>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 com-sm-12">
                        <a href="{{URL::previous()}}" class="btn btn-form form-control">Voltar</a>
                    </div>
                    <div class="col-md-6 com-sm-12"></div>
                    <div class="col-md-3 com-sm-12 text-right">
                        <button type="submit" class="btn btn-form form-control">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endcomponent
<script type="text/javascript" src="{{ asset('js/city/selectCity.js')}}"></script>
<script type="text/javascript">
    let uf = $('#uf');
    let city = $('#cidade');
    let Cocity = $('#coCidade');

    selectCity(city, uf.val(),Cocity.val());

    uf.change(() => {
        selectCity(city, uf.val())
    });
</script>