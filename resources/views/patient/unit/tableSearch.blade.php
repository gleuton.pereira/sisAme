<div class="table-responsive">
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>Unidades de Atendimento</th>
            <th class="text-sm-center">Detalhes</th>
        </tr>
        </thead>
        <tbody>
        @foreach($unidades as $unidade)
            <tr>
                <td>{{$unidade->NOME_FANTAZIA}}</td>
                <td class="text-sm-center">
                    <a href="/paciente/unidade/{{$unidade->CO_UNIDADE_ATENDIMENTO}}">
                        <span class="material-icons">room</span>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>