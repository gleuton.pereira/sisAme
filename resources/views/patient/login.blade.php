@component('patient.layouts.default')
    @slot('title') Login @endslot
    <section class="login" id="login">
        <div class="container text-center">
            <h1>Login</h1>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <form action="{{action('LoginController@login')}}" method="post">
                        @csrf
                        <div class="form-group form-adm text-center">
                            <label>E-Mail:</label>
                            <input type="email" class="form-control" id="user-login"
                                   placeholder="Digite o E-mail"
                                   name="email"
                                   maxlength="100"
                                   minlength="4"
                                   required>
                            <label>Senha:</label>
                            <input type="password" class="form-control" id="senha-login"
                                   name="senha"
                                   placeholder="Senha"
                                   maxlength="100"
                                   minlength="4"
                                   required>
                        </div>
                        <div class="form-group">

                                @if(isset($erro))
                                    <div class="alert alert-danger" role="alert">
                                        {{$erro}}
                                    </div>
                                @endif

                        </div>
                        <div class="row">
                            <div class="col-md-6"></div>

                            <div class="col-md-6 com-sm-12 text-center">
                                <button type="submit" class="btn btn-form form-control btn-logar">Logar</button>
                            </div>
                        </div>
                    </form>
                    @if(count($errors) > 0)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="text-center" style="margin-top: 40px;">
                        <h4>Não possui cadastro?</h4>
                        <a href="/paciente/cadastro" class="btn btn-cadastro">Criar Conta</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>
@endcomponent