<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Symptom extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_SINTOMA';

    protected $primaryKey = 'CO_SINTOMA';

    protected $guarded = ['CO_SINTOMA'];

    public $timestamps = false;

    protected $fillable = [
        'INTENSIDADE',
        'CO_TIPO_SINTOMA',
        'CO_PRETRIAGEM'
    ];
}
