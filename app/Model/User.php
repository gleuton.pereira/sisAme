<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_USUARIO';

    protected $primaryKey = 'CO_USUARIO';

    protected $guarded = ['CO_USUARIO'];

    protected $dates = ['DT_NASCIMENTO'];

    public $timestamps = false;

    protected $fillable = [
        'RG',
        'CPF',
        'SEXO',
        'NOME',
        'SOBRENOME',
        'DT_NASCIMENTO',
        'SENHA',
        'CO_TIPO_USUARIO',
        'CO_TIPO_SANGUINEO',
        'CO_CARTAO',
        'CO_PROBLEMA_SAUDE',
        'CO_CONTATO',
        'CO_ENDERECO'
    ];
}
