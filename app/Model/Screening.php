<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Screening extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_PRETRIAGEM';

    protected $primaryKey = 'CO_PRETRIAGEM';

    protected $guarded = ['CO_PRETRIAGEM'];

    protected $dates = ['DT_ATENDIMENTO'];

    public $timestamps = false;

    protected $fillable = [
        'DT_ATENDIMENTO',
        'CO_UNIDADE_ATENDIMENTO',
        'CO_USUARIO'
    ];
}
