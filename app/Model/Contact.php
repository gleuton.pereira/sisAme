<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $connection = 'mysql';

    protected $table = 'TB_CONTATO';

    protected $primaryKey = 'CO_CONTATO';

    protected $guarded = ['CO_CONTATO'];

    public $timestamps = false;

    protected $fillable = [
        'EMAIL'
    ];
}
