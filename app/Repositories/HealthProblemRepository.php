<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;


use App\Model\HealthProblem;

class HealthProblemRepository
{
    private $model;

    public function __construct(HealthProblem $problem)
    {
        $this->model = $problem;
    }

    public function getAll()
    {
        return $this->model->orderBy('PROBLEMA')->get();
    }

}