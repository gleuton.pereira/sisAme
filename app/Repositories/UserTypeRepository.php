<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;

use App\Model\UserType;


class UserTypeRepository
{
    private $model;

    public function __construct(UserType $userType)
    {
        $this->model = $userType;
    }

    public function getAll()
    {
        return $this->model->all()->sortBy('TIPO');
    }
    public function getByType($type)
    {
        return $this->model->where('TIPO',$type)->first();
    }
}