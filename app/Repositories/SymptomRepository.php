<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;

use App\Model\Symptom;


class SymptomRepository
{
    private $model;

    public function __construct(Symptom $symptom)
    {
        $this->model = $symptom;
    }

    public function create(int $co_triagem, array $data)
    {

        $this->model->CO_TIPO_SINTOMA = intval($data['sintoma']);
        $this->model->CO_PRETRIAGEM = intval($co_triagem);
        $this->model->INTENSIDADE = intval($data['intensidade']);
        $this->model->save();
        return $this->model->CO_SINTOMA;
    }
}