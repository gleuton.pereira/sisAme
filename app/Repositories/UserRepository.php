<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 26/05/18
 */

namespace App\Repositories;

use App\Model\User;


class UserRepository
{
    private $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function create(array $data)
    {
        $this->model->NOME = $data['nome'];
        $this->model->SOBRENOME = $data['sobrenome'];
        $this->model->RG = $data['rg'];
        $this->model->CPF = $data['cpf'];
        $this->model->DT_NASCIMENTO = $data['dt_nascimento'] ?? null;
        $this->model->SEXO = $data['sexo'];
        $this->model->SENHA = password_hash($data['senha'], PASSWORD_DEFAULT);
        $this->model->CO_TIPO_USUARIO = $data['co_tipo_usuario'];
        $this->model->CO_TIPO_SANGUINEO = $data['tipo_sanguineo'];
        $this->model->CO_CARTAO = $data['cartao'] ?? null;
        $this->model->CO_PROBLEMA_SAUDE = $data['problema_saude'] ?? null;
        $this->model->CO_CONTATO = $data['co_contato'];
        $this->model->CO_ENDERECO = $data['co_endereco'];

        $this->model->save();
        return $this->model->CO_USUARIO;
    }

    public function update(array $data, int $coUser)
    {
        $user = $this->model->find($coUser);


        $user->NOME = $data['nome'];
        $user->SOBRENOME = $data['sobrenome'];
        $user->DT_NASCIMENTO = $data['dt_nascimento'] ?? null;
        $user->SEXO = $data['sexo'];

        if(!empty($data['senha'])){
            $user->SENHA = password_hash($data['senha'], PASSWORD_DEFAULT);
        }

        $user->CO_TIPO_SANGUINEO = $data['tipo_sanguineo'];
        $user->CO_CARTAO = $data['cartao'] ?? null;
        $user->CO_PROBLEMA_SAUDE = $data['problema_saude'] ?? null;

        $user->save();

        return $user->CO_USUARIO;
    }

    public function getAll()
    {
        return $this->model->all()->sortBy('CO_USUARIO');
    }

    public function getUser($coUser)
    {
        return $this->model
            ->join('TB_ENDERECO', 'TB_USUARIO.CO_ENDERECO', 'TB_ENDERECO.CO_ENDERECO')
            ->join('TB_CIDADE', 'TB_ENDERECO.CO_CIDADE', 'TB_CIDADE.CO_CIDADE')
            ->join('TB_ESTADO', 'TB_CIDADE.CO_ESTADO', 'TB_ESTADO.CO_ESTADO')
            ->join('TB_CONTATO', 'TB_USUARIO.CO_CONTATO', 'TB_CONTATO.CO_CONTATO')
            ->join('TB_TELEFONE', 'TB_CONTATO.CO_CONTATO', 'TB_TELEFONE.CO_CONTATO')
            ->where('CO_USUARIO', $coUser)
            ->first(['TB_USUARIO.*',
                'TB_CONTATO.*',
                'TB_ENDERECO.*',
                'TB_TELEFONE.*',
                'TB_CIDADE.CO_CIDADE',
                'TB_ESTADO.CO_ESTADO'
            ]);
    }

    public function getByEmail($email)
    {
        return $this->model
            ->join('TB_CONTATO', 'TB_USUARIO.CO_CONTATO', 'TB_CONTATO.CO_CONTATO')
            ->join('TB_TIPO_USUARIO', 'TB_USUARIO.CO_TIPO_USUARIO', 'TB_TIPO_USUARIO.CO_TIPO_USUARIO')
            ->where('TB_CONTATO.EMAIL', $email)
            ->first();
    }
}