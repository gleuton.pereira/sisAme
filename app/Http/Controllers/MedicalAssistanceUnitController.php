<?php
/**
 * Project: sisAme
 * User: gleuton
 * Date: 31/05/18
 */

namespace App\Http\Controllers;


use App\Model\Specialty;
use App\Model\State;
use App\Repositories\UnityRepository;


class MedicalAssistanceUnitController extends Controller
{
    /**
     * @var State estados
     */
    private $modelState;

    /**
     * @var Specialty especialidades
     */
    private $modelSpecialty;
    /**
     * @var UnityRepository
     */
    private $repositoryUnity;

    /**
     * MedicalAssistanceUnitController constructor.
     * @param State $state
     * @param Specialty $specialty
     * @param UnityRepository $repositoryUnity
     */
    public function __construct(
        State $state,
        Specialty $specialty,
        UnityRepository $repositoryUnity
    )
    {
        $this->modelState = $state;
        $this->modelSpecialty = $specialty;
        $this->repositoryUnity = $repositoryUnity;
    }

    /**
     * @return string view da pagina incial
     */
    public function index(): string
    {
        $estados = $this->modelState->all()->sortBy('UF');
        $especialidades = $this->modelSpecialty->all()->sortBy('ESPECIALIDADE');
        return view('patient.unit.formSearch')
            ->with(compact('estados'))
            ->with(compact('especialidades'));
    }

    /**
     * @param string $json paramestros das buscas
     * @return string view da tabela com as unidades
     *
     */
    public function search(string $json): string
    {
        $search = [];
        foreach (json_decode($json) as $iten) {
            $search[$iten->name] = $iten->value;
        }
        if (empty($search['cidade'])) {
            return '<div class="alert alert-warning" role="alert">Informe Todos os campos da busca!</div>';
        }
        $unidades = $this->repositoryUnity->search($search['cidade'], $search['espcialidade']);

        if (empty($unidades->all())) {
            return '<div class="alert alert-warning" role="alert">Não foram encontradas Unidades de Atendimento.</div>';
        }
        return view('patient.unit.tableSearch')
            ->with(compact('unidades'));
    }

    /**
     * @param int $id identidicador da unidade de atendimento
     * @return string view do detalhemento das unidades
     */
    public function getUnity(int $id): string
    {
        $unidade = $this->repositoryUnity->getUnity($id);

        return view('patient.unit.details')
            ->with(compact('unidade'));
    }
}